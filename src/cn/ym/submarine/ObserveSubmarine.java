package cn.ym.submarine;

import javax.swing.ImageIcon;

public class ObserveSubmarine extends SeaObject implements EnemyLife{
    public ObserveSubmarine(){
        super(63,19,9);
    }

    @Override
    public void move() {
        this.x += this.speed;
    }

    @Override
    public ImageIcon getImage() {
        return Images.obsersubm;
    }

    @Override
    public int getLife() {
        return 1;
    }
}
