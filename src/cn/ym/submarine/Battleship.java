package cn.ym.submarine;

import javax.swing.ImageIcon;

public class Battleship extends SeaObject {
    private int life;

    public Battleship() {
        super(66, 26, 200, 124, 20);
        life = 5;
    }

    @Override
    public void move() {

    }

    @Override
    public ImageIcon getImage() {
        return Images.battleship;
    }

    /**
     * 战舰左移动
     *
     * @author xw
     */
    public void moveLeft() {
        this.x -= speed;
        if (this.x - speed <= 0) this.x = 0;
    }

    /**
     * 战舰右移动
     *
     * @author xw
     */
    public void moveRight() {
        this.x += speed;
        if (this.x + speed >= World.WIDTH - this.width) this.x = World.WIDTH - this.width;
    }

    /**
     * 发射炸弹
     *
     * @return Bomb
     * @author xw
     */
    public Bomb shoot() {
        int x = this.x + this.width / 2;
        int y = this.y + this.height;
        return new Bomb(x, y);
    }

    /**
     * 战舰增加命
     *
     * @author xw
     */
    public void addLife(int num) {
        life += num;
    }

    /**
     * 战舰当前命
     *
     * @return int
     * @author xw
     */
    public int getLife() {
        return this.life;
    }

    /**
     * 战舰减少命
     *
     * @author xw
     */
    public void subtractLife() {
        life--;
    }

}
