package cn.ym.submarine;

import javax.swing.ImageIcon;

public class MineSubmarine extends SeaObject implements EnemyScore {
    public MineSubmarine() {
        super(63, 19, 6);
    }

    @Override
    public void move() {
        this.x += this.speed;
    }

    @Override
    public ImageIcon getImage() {
        return Images.minesubm;
    }

    /**
     * 发射水雷
     *
     * @return Mine
     * @author xw
     */
    public Mine shootMine() {
        int x = this.x + this.width;
        int y = this.y - 11;
        return new Mine(x, y);
    }

    @Override
    public int getScore() {
        return 10;
    }
}
