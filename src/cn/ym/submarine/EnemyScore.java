package cn.ym.submarine;

public interface EnemyScore {
    /**
     * 奖励分数
     *
     * @return int
     * @author xw
     */
    public abstract int getScore();
}
