package cn.ym.submarine;

public interface EnemyLife {
    /**
     * 奖励命数
     *
     * @return int
     * @author xw
     */
    public abstract int getLife();
}
