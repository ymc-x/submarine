package cn.ym.submarine;

import javax.swing.ImageIcon;

public class Images {
    protected static ImageIcon battleship;
    protected static ImageIcon bomb;
    protected static ImageIcon gameover;
    protected static ImageIcon mine;
    protected static ImageIcon minesubm;
    protected static ImageIcon obsersubm;
    protected static ImageIcon sea;
    protected static ImageIcon torpesubm;

    static {
        Images.battleship = new ImageIcon("img/battleship.png");
        Images.bomb = new ImageIcon("img/bomb.png");
        Images.gameover = new ImageIcon("img/gameover.png");
        Images.mine = new ImageIcon("img/mine.png");
        Images.minesubm = new ImageIcon("img/minesubm.png");
        Images.obsersubm = new ImageIcon("img/obsersubm.png");
        Images.sea = new ImageIcon("img/sea.png");
        Images.torpesubm = new ImageIcon("img/torpesubm.png");
    }

}
