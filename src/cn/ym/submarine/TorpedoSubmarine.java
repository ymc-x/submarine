package cn.ym.submarine;

import javax.swing.ImageIcon;

public class TorpedoSubmarine extends SeaObject implements EnemyScore{
    public TorpedoSubmarine(){
        super(64,20,3);
    }

    @Override
    public void move() {
        this.x += this.speed;
    }

    @Override
    public ImageIcon getImage() {
        return Images.torpesubm;
    }

    @Override
    public int getScore() {
        return 20;
    }
}
