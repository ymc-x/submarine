package cn.ym.submarine;

import javax.swing.ImageIcon;

public class Bomb extends SeaObject{
    public Bomb(int x,int y){
        super( 11, 11,x, y, 4);
    }

    @Override
    public void move() {
        this.y += this.speed;
    }

    @Override
    public ImageIcon getImage() {
        return Images.bomb;
    }

    public boolean isOutOfBounds(){
        return this.y >= World.HEIGHT;
    }
}
